#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from 'aws-cdk-lib';
import * as blueprints from '@aws-quickstart/eks-blueprints';
import { ClusterLoggingTypes, KubernetesVersion } from 'aws-cdk-lib/aws-eks';
import { GitLabRunner, CpuArch } from '../lib/gitlab-runner-addon';
import { CoreDNSFargatePatch } from '../lib/coredns-patcher';

const app = new cdk.App();

const clusterProvider = new blueprints.GenericClusterProvider({
  version: KubernetesVersion.V1_23,
  fargateProfiles: {
    main: {
      selectors: [
        { namespace: 'kube-system' },
        { namespace: 'karpenter' },
      ]
    }
  },
  clusterLogging: [
    ClusterLoggingTypes.API,
    ClusterLoggingTypes.AUDIT,
    ClusterLoggingTypes.AUTHENTICATOR,
    ClusterLoggingTypes.CONTROLLER_MANAGER,
    ClusterLoggingTypes.SCHEDULER
  ]
});

const blueprint = blueprints.EksBlueprint.builder()
  .clusterProvider(clusterProvider)
  .addOns(
    new CoreDNSFargatePatch(),
    new blueprints.addons.CoreDnsAddOn('v1.8.7-eksbuild.3'),
    new blueprints.addons.VpcCniAddOn('v1.12.0-eksbuild.1'),
    new blueprints.addons.KarpenterAddOn({
      requirements: [
        {
          key: 'kubernetes.io/arch',
          op: 'In',
          vals: ['amd64', 'arm64']
        },
        {
          key: 'karpenter.sh/capacity-type',
          op: 'In',
          vals: ['spot', 'on-demand']
        }
      ],
      subnetTags: {
        Name: 'gitlab-runner-eks-demo/gitlab-runner-eks-demo-vpc/PrivateSubnet*'
      },
      securityGroupTags: {
        'kubernetes.io/cluster/gitlab-runner-eks-demo': 'owned'
      }
    }),
    new GitLabRunner({
      arch: CpuArch.ARM_64,
      gitlabUrl: 'https://gitlab.com',
      secretName: 'gitlab-runner-secret'
    }),
    new GitLabRunner({
      arch: CpuArch.X86_64,
      gitlabUrl: 'https://gitlab.com',
      secretName: 'gitlab-runner-secret'
    }),
  )
  .build(app, 'gitlab-runner-eks-demo');
